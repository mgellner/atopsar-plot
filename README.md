# Atopsar-Plot

Graphically represent the historical system resource consumption like CPU, disk, memory and network usage in a terminal.

![Example output](src/dc4fd016f61c7472.png)

## Features

Atopsar-Plot allows graphing various historic system information directly in the terminal. It is based on logs written 
by [atop](https://github.com/Atoptool/atop). The following information can be plotted:

* %CPU_busy: CPU busy time in percent. Multiple cores are averaged to 100%
* %DSK_busy: Disk activity in percent (see --disk flag)
* %MEM_used: Used memory in percent ((memtotal - memfree - buffers - cached) / memtotal * 100)
* %NET_busy: Network usage in percent. Formula: max(imbps, ombps) / maxmbps_if_ (see --iface flag)

Plots for previous days can be plotted with the parameter --day -1, --day -2, ... or with alternative format YYYY-MM-DD

### Resource usage alerts

Resource usage alerts can be sent by specifying an --alert-mail. For this to work, the package mailx must be installed
and configured to allow sending mails. You may create a cron job for alerts. Keep in mind that atop averages resource
consumption over the whole time granularity (default is 10 minutes). With the default --alert-threshold (70) an alert
will be generated if any of the monitored resources exceeds 70% of its maximum capacity.

Additionally, there is a hard disk space allocation alert for the root filesystem "/" whose threshold can be set 
with --alert-disk-threshold.

    # crontab -e
    */10 * * * * /path/to/atopsar-plot --alert-mail alert@example.com


## Installing

### Install atop
Install via package manager

    sudo apt install atop

Or download and install from source [atop](https://www.atoptool.nl/downloadatop.php) and netatop on a debian based system

    sudo apt install zlib1g-dev zlib1g libncurses5-dev
    wget https://www.atoptool.nl/download/netatop-3.1.tar.gz
    tar -xzf netatop*.gz
    cd netatop*/
    make -j4
    sudo make install
    sudo systemctl enable --now netatop
    cd ..
    wget https://www.atoptool.nl/download/atop-2.9.0.tar.gz
    tar -xzf atop*.gz
    cd atop*/
    make -j4
    sudo make install
    sudo systemctl enable --now atop
    sudo systemctl enable --now atop-rotate.timer

### Install atopsar-plot and PIP dependencies

    pip install -U git+https://codeberg.org/mgellner/atopsar-plot

## Usage
    
    atopsar-plot --help
    atopsar-plot --day 2024-02-06 --disk vda --iface eth0

## Contributing

When contributing to this repository, please first discuss the change you wish to make via issue

## License

This project is licensed under the Apache-2.0 License - see the [LICENSE](LICENSE) file for details

## Contact

See [https://www.miaplan.de/](https://www.miaplan.de/)

## Similar projects

* [https://github.com/fnep/aplot](https://github.com/fnep/aplot)
* [https://github.com/shuyufu/atop-plot](https://github.com/shuyufu/atop-plot)
