#!/usr/bin/env python
'''
ATOPSAR-PLOT - Historical system resource usage visualizer

Notes
    atop -crb 0430
    atop -PCPU,DSK -r
    atop -PCPU,DSK,NET -rb 1820
    atop -PPRG -rb 1820
    atop -crb 0430|head -n30
    atop -c|head -n30

@license: Apache-2.0
@author: mgellner
@copyright: 2022 mgellner
@organization: miaplan.de
'''

import argparse
from dataclasses import dataclass
from datetime import datetime as dt, timedelta as td
from functools import reduce
from operator import itemgetter
import operator
import os
import re
import socket
import sys
from typing import Callable, Dict

import multiprocessing as mp
import plotext as plt
import subprocess as sp  # pylint: disable=wrong-import-order

MAIL_SUBJECT_PREFIX = '[resource usage alert]'


@dataclass
class LogAttr:
    ''' Log entry attribute '''
    LineFilter: Callable[[str], bool]
    UsageAlert: Callable[[int], bool]
    ExtractColumns: Dict[str, Callable[[str], int]]


class LogEntry:
    ''' Log entry constants '''

    def __init__(self, args):

        n_cpu_cores = mp.cpu_count()
        with open('/proc/meminfo') as f:
            total_mem_mb = int(f.read().split()[1]) / 1024

        try:
            with open(f'/sys/class/net/{args.iface}/speed') as f:
                iface_speed_mbps = int(f.read().strip())
        except OSError:  # default to 1000 MBit
            iface_speed_mbps = 1000

        iface_speed_mbps = 1000 if iface_speed_mbps == -1 else iface_speed_mbps  # -1 without root in some cases

        if args.disk == 'auto':
            try:
                out = sp.run(f'lsblk -fdETYPE'.split(), stdout=sp.PIPE, stderr=sp.PIPE, check=True, text=True)
                disk = out.stdout.split(os.linesep)[-2].rstrip()
            except sp.CalledProcessError:
                disk = 'sda'
        else:
            disk = args.disk

        self.CPU_busy = LogAttr(
            lambda line: ' all' in line,
            lambda last_value: last_value >= args.alert_threshold,
            {'%CPU_busy': lambda entry: 100 - (float(entry[-1]) / n_cpu_cores)})
        self.MEM_used = LogAttr(
            lambda line: re.search(r'\d\dM', line),
            lambda last_value: last_value >= args.alert_threshold,
            {'%MEM_used': lambda entry: reduce(
                operator.sub, [float(value[:-1]) for value in entry[1:5]]) / total_mem_mb * 100})
        self.DSK_busy = LogAttr(
            lambda line: re.search(fr'\d\s+{disk}', line),
            lambda last_value: last_value >= args.alert_threshold,
            {'%DSK_busy': lambda entry: float(entry[2][:-1])})
        self.NET = LogAttr(
            lambda line: re.search(fr'\d\s+{args.iface.replace("enp", "")}', line),
            lambda last_value: last_value >= args.alert_threshold,
            {'%NET_busy': lambda entry: max(float(entry[7]), float(entry[8])) * 100 / iface_speed_mbps})


class AtopLog:
    ''' Log item for atopsar log entries '''

    def __init__(self, log_attrs):
        self.Time = []

        for log_attr in log_attrs:
            for name in log_attr.ExtractColumns.keys():
                setattr(self, name.replace('%', ''), [])


def parse_atopsar_out(log_attrs, args):
    ''' Parse atopsar log output '''

    try:
        day_dt = dt.today() - td(days=-int(args.day))
    except ValueError:  # YYYY-MM-DD format
        day_dt = dt.strptime(args.day, '%Y-%m-%d')

    atop_log = AtopLog(log_attrs)

    try:
        log = sp.run(f'atopsar -r {day_dt.strftime("%Y%m%d")} -cmdiSax'.split(),
                     stdout=sp.PIPE, stderr=sp.PIPE, check=True, text=True)
    except sp.CalledProcessError as e:
        print(e.stderr.rstrip(), file=sys.stderr)
        sys.exit()

    for log_attr in log_attrs:
        log_split = [line.split() for line in log.stdout.split(os.linesep) if log_attr.LineFilter(line)]

        for name, func in log_attr.ExtractColumns.items():
            setattr(atop_log, name.replace('%', ''), [func(entry) for entry in log_split])

    atop_log.Time = [entry[0] for entry in log_split]

    return atop_log


def plot_graphs(args, log_attrs, atop_log):
    ''' Plot graphs from atop logs '''
    for log_attr in log_attrs:
        for name in log_attr.ExtractColumns.keys():
            plt.clf()
            values = getattr(atop_log, name.replace('%', ''))
            plt.ylim(min(values), max(values) + 1)
            plt.plot_size(*args.plot_size)
            plt.title(name)
            time_int = [int(t[:-3].replace(':', '')) for t in atop_log.Time]
            plt.xticks(time_int, [t[:-3] for t in atop_log.Time])
            plt.plot(time_int, values, fillx=True)
            plt.show()
            print()


def get_default_iface_name():
    ''' Get default network interface name
    @see: https://stackoverflow.com/a/20925510 '''

    with open('/proc/net/route') as f:
        for line in f.readlines():
            try:
                iface, dest, flags = itemgetter(0, 1, 3)(line.strip().split())
                if dest != '00000000' or not int(flags, 16) & 2:
                    continue
            except Exception:  # pylint: disable=try-except-raise
                raise

            return iface


def check_resource_alert(args, log_attrs, atop_log):
    ''' Check resource usage and send alert mail '''

    def get_atop_out():
        ''' Get atop -c stdout '''
        max_cmds = -1
        atop_out = ''
        with sp.Popen(['atop', '-c'], stdout=sp.PIPE) as atop:
            for i, line in enumerate(atop.stdout):
                if not line.startswith(b'cpu'):
                    atop_out += line.decode()
                if b'COMMAND-LINE' in line:
                    max_cmds = i + 10  # catch only the first 10 processes
                if i == max_cmds:
                    break
        return atop_out

    for log_attr in log_attrs:
        for name in log_attr.ExtractColumns.keys():
            res_name = name.replace('%', '')

            try:
                last_value = getattr(atop_log, res_name)[-1]
            except IndexError:
                print(f'atopsar log file for day {args.day} is empty', file=sys.stderr)
                sys.exit()

            host = socket.gethostname()

            if log_attr.UsageAlert(last_value):
                subject = f'{MAIL_SUBJECT_PREFIX} {res_name}={last_value} for {host}'
                print(subject)
                sp.run(['mailx', '-s', subject, args.alert_mail], input=(
                    f'{name} on {host} exceeded the utilization threshold: value={last_value} '
                    f'threshold={args.alert_threshold}\n{get_atop_out()}').encode(), check=True)

    if args.alert_disk_threshold:
        root_used_percent = int(
            sp.run(['df', '--output=pcent', '/'], check=True, stdout=sp.PIPE, text=True).stdout.split()[-1][:-1])

        if root_used_percent >= args.alert_disk_threshold:
            subject = f'{MAIL_SUBJECT_PREFIX} root_fs={root_used_percent}% for {host}'
            print(subject)
            sp.run(['mailx', '-s', subject, args.alert_mail], input=(
                    f'Root file system space allocation on {host} exceeded the threshold: value={root_used_percent} '
                    f'threshold={args.alert_disk_threshold}\n{get_atop_out()}').encode(), check=True)


def main():
    ''' main '''

    parser = argparse.ArgumentParser(description=__doc__.split('\n')[1].strip())  # @UndefinedVariable
    parser.add_argument('-d', '--day', default=0,
                        help='day of atopsar log file. 0: today, -1: yesterday, ... Alternative format: '
                             'YYYY-MM-DD (default: %(default)s)')
    parser.add_argument('-r', '--disk', default='auto',
                        help='name of the hard disk drive (e.g. "sda". "auto" will try to auto-determine the drive '
                        'default: %(default)s)')
    parser.add_argument('-i', '--iface', default=get_default_iface_name(),
                        help='name of the network interface (default: %(default)s)')
    parser.add_argument('-s', '--plot-size', default=(80, 8), nargs=2, metavar=('width', 'height'),
                        help='Plot dimensions for single plot in pixels (default: %(default)s)')
    parser.add_argument('-a', '--alert-mail', default='',
                        help='E-Mail address for resource usage alerts (default: %(default)s)')
    parser.add_argument('-t', '--alert-threshold', default=70, type=int, choices=range(0, 101), metavar='[0-100]',
                        help='Resource usage alert threshold percent (default: %(default)s)')
    parser.add_argument('-f', '--alert-disk-threshold', default=90, type=int, choices=range(0, 101), metavar='[0-100]',
                        help='Hard disk space allocation alert threshold percent for root filesystem "/". '
                             'Only used when --alert-mail is set (default: %(default)s)')
    args = parser.parse_args(sys.argv[1:])
    log_entry = LogEntry(args)
    log_attrs = [getattr(log_entry, entry) for entry in dir(log_entry) if not entry.startswith('_')]

    if args.alert_mail:
        args.day = 0  # we only want recent resource usage for alerts

    atop_log = parse_atopsar_out(log_attrs, args)

    if args.alert_mail:
        check_resource_alert(args, log_attrs, atop_log)
        return  # don't plot in alert mode

    try:
        plot_graphs(args, log_attrs, atop_log)
    except ValueError as e:
        if str(e) == 'min() arg is an empty sequence':
            print('no atop log entries yet or wrong disk name (default=sda)')


if __name__ == '__main__':
    main()
