from setuptools import setup

with open('README.md', 'r') as f:
    long_description = f.read()

setup(
    name='atopsar-plot',
    version='0.3.2',
    author='mgellner',
    author_email='',
    description='ATOPSAR-PLOT - Yet another atop log visualizer',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://codeberg.org/mgellner/atopsar-plot',
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Topic :: System :: Monitoring',
        'Topic :: Utilities',
    ],
    py_modules=['atopsar_plot'],
    entry_points={'console_scripts': ['atopsar-plot=atopsar_plot:main']},
    python_requires='>=3.7',
    install_requires=['plotext'],
)
